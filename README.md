au.com.supportingcast.build.unity

https://docs.unity3d.com/Manual/AssemblyDefinitionFileFormat.html
https://docs.unity3d.com/Manual/cus-document.html

Developer package documentation. 

This is generally documentation to help developers who want to modify the package or push a new change on the package master source repository.

For Systems with Unity Hub on macOS:
/Applications/Unity/2020.3.0f1/Unity.app/Contents/MacOS/Unity -batchmode -nographics -quit -logFile - -executeMethod AutoBuilder.PerformAndroidBuild

For Systems with standalone Unity installations on macOS:
/Applications/Unity/Unity.app/Contents/MacOS/Unity -batchmode -quit -projectPath ProjectPath -executeMethod FileName.StaticMethodName

Unity Hub on Windows: 
C:\Program Files\Unity\Hub\Editor\2019.2.7f1\Editor\Unity.exe -batchmode -nographics -quit -logFile - -executeMethod BatchModeEditorUtility.BuildiOS
