Description of package changes in reverse 
chronological order. It is good practice to use a 
standard format, like Keep a Changelog.

