﻿using UnityEngine;
using System;
using System.Text;
using UnityEngine.Assertions;
using Discord;

public enum DiscordInitialisationStatus {
	error,
	initialised,
	uninitialised
}

public class DiscordManager : MonoBehaviour
{
    [SerializeField] private TextAsset config;

    // Do nothing for mobile platforms
    #if UNITY_IOS
    #else
    
    // Todo: Opt-out setting
    private Discord.Discord _discord;
	public DiscordInitialisationStatus _status = DiscordInitialisationStatus.uninitialised;

    private Activity _launchActivity;
    
    // Update user's activity for your game.
    // Read https://discordapp.com/developers/docs/rich-presence/how-to for more details.
    private void Start()
    {
		try {
        	DiscordAppConfig configObject = DiscordAppConfig.fromJson(config.text);
        	_launchActivity = new Activity
        	{
            	Details = configObject.initialPresenceDetails,
            	Timestamps =
            	{
            	    Start = 0
            	},
            	Assets =
            	{
            	    LargeImage = configObject.LargeImage,
            	    LargeText = configObject.LargeText,
            	    SmallImage = configObject.SmallImage,
            	    SmallText = configObject.SmallText,
           		},
        	};

        	// Use your client ID from Discord's developer site.
        	var clientID = configObject.id;

        	_discord = new Discord.Discord(Int64.Parse(clientID), (UInt64) CreateFlags.NoRequireDiscord);
        	_discord?.SetLogHook(LogLevel.Debug, (level, message) =>
        	{
        	    Console.WriteLine("Log[{0}] {1}", level, message);
        	});

        	// Set initial activity
        	updateActivity(_launchActivity);
			_status = DiscordInitialisationStatus.initialised;
		} catch (Exception error) {
			_status = DiscordInitialisationStatus.error;
			Debug.Log(error);
		}
    }

    void clearActivity()
    {
        var activityManager = _discord?.GetActivityManager();

        activityManager?.ClearActivity(result =>
        {
            if (result != Result.Ok)
            {
                print("Error from discord (" + result.ToString() + ")");
            }
            else
                print("Discord Result = " + result.ToString());
        });
    }

    void updateActivity(Activity activity)
    {
        var activityManager = _discord?.GetActivityManager();

        activityManager?.UpdateActivity(activity, result =>
        {
            if (result != Result.Ok)
            {
                print(result);
                print("Error from discord (" + result.ToString() + ")");
            }
            else
                print("Discord Result = " + result.ToString());
        });
    }

    void FixedUpdate()
    {
		if (_status == DiscordInitialisationStatus.initialised) {
        	// Pump the event look to ensure all callbacks continue to get fired.
        	_discord?.RunCallbacks();
		} 
    }

    // Note: Currently experience delay of up to 30 seconds after
    // application end before this clears
    private void OnDestroy()
    {
        if (this.gameObject.activeSelf)
        {
            clearActivity();

            _discord?.RunCallbacks();
            _discord?.Dispose();
        }
    }
    
    #endif
}
