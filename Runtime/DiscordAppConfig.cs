using System;
using UnityEngine;

[Serializable]
public class DiscordAppConfig
{
    DiscordAppConfig(
        string id, 
        string details, 
        string largeImage, 
        string largeText, 
        string smallImage, 
        string smallText
    )
    {
        this.id = id;
        this.initialPresenceDetails = details;
        this.LargeImage = largeImage;
        this.LargeText = largeText;
        this.SmallImage = smallImage;
        this.SmallText = smallText;
    }
    
    public static DiscordAppConfig fromJson(string jsonString)
    {
        return JsonUtility.FromJson<DiscordAppConfig>(jsonString);
    }

    public string id;
    public string initialPresenceDetails;
    public string LargeImage;
    public string LargeText;
    public string SmallImage;
    public string SmallText;
}